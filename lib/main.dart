import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Интернет-технологии", style: TextStyle(color: Colors.white, fontSize: 24),),
          backgroundColor: Color.fromARGB(255, 39, 36, 36),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomCenter,
                child: Image.asset('photo/stankin.png'),
              ),
              Text(
                'Лабораторная работа №1',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 30),
              ),
              Padding(
                  padding: EdgeInsets.all(40),
                  child: Align(
                    child: Column(
                      children: <Widget>[
                        Text(
                            'GitHub — крупнейший веб-сервис для хостинга IT-проектов и их совместной разработки. Веб-сервис основан на системе контроля версий Git и разработан на Ruby on Rails и Erlang компанией GitHub, Inc.',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                        Text(
                            'План выполнения В рамках данной лабораторной работы необходимо:                                                                                                                                                                                                                                                             ',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                        Text(
                            '1. Зарегистрироваться на github.com;                                                                                                                                                                                                                                                                                                                                                                                                               ',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                        Text(
                            '2. Cоздать личный репозиторий;                                                                                                                                                                                                                                                                                                                                                                 ',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                        Text(
                            '3. Cоздать личную веб-страницу с описанием выполнения лабораторных работ;                                                                                                                                                                                                                                                                                                                         ',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                        Text(
                            '4. Разместить код веб-страницы в репозитории;                                                                                                                                                                                                                                                                                                                                                                                     ',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                        Text(
                            '5. Разместить код веб-страницы на хостинге.                                                                                                                                                                                                                                                                                                                                                                                     ',
                            style:
                                TextStyle(color: Colors.black, fontSize: 24)),
                      ],
                    ),
                  )),
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    color: Colors.black,
                    alignment: Alignment.center,
                    child: Text('Арсеньев Даниэль ИДМ-22-01', style:
                                TextStyle(color: Colors.white, fontSize: 24)),
                  )
            ],
          ),
        ),
      ),
    );
  }
}
